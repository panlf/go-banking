package main

import (
	"go-banking/app"
	"go-banking/logger"
)

func main() {

	logger.Info("Starting the application...")

	app.Start()
}
