CREATE TABLE `accounts` (
  `account_id` int NOT NULL AUTO_INCREMENT,
  `customer_id` int NOT NULL,
  `opening_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `account_type` varchar(10) DEFAULT NULL,
  `amount` decimal(10,2) NOT NULL,
  `status` tinyint NOT NULL DEFAULT '1',
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;


CREATE TABLE `customers` (
  `customer_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `date_of_birth` date NOT NULL,
  `city` varchar(100) NOT NULL,
  `zipcode` varchar(10) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;


INSERT INTO banking.accounts (`customer_id`,`opening_date`,`account_type`,`amount`,`status`) VALUES
	 (1,'2020-08-22 10:20:06','Saving',1075.00,1),
	 (2,'2020-06-15 10:00:00','Saving',1255.00,1),
	 (3,'2020-08-09 10:00:00','Checking',763.00,1),
	 (4,'2020-06-03 10:00:00','Saving',310.00,1),
	 (5,'2020-02-27 10:00:00','Checking',4838.00,1),
	 (6,'2020-08-09 10:00:00','Saving',2078.00,0),
	 (1,'2022-05-14 22:12:19','Saving',5000.23,1);


INSERT INTO banking.customers (`name`,`date_of_birth`,`city`,`zipcode`,`status`) VALUES
	 ('Steve','1978-12-15','Delhi','110075',1),
	 ('Arian','1988-05-21','Newburgh,NY','12550',1),
	 ('Hadley','1988-04-30','Englewood,NJ','07631',1),
	 ('Ben','1988-01-04','Manchester,NH','03102',0),
	 ('Nina','1988-05-14','Clarkston,MI','48348',1),
	 ('Osman','1988-11-08','Hyattsville,MD','20782',0);

